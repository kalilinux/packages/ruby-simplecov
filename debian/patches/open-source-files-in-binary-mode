From 6abb79e6b3eeed02997cb356cf73647e97be0edc Mon Sep 17 00:00:00 2001
From: =?UTF-8?q?C=C3=A9dric=20F=C3=A9lizard?= <cedric@felizard.fr>
Date: Tue, 12 Feb 2013 17:19:43 +0100
Subject: [PATCH] Open source files in binary mode

Opening non-UTF-8 files as UTF-8 succeeds in some cases and will throw
<ArgumentError: invalid byte sequence in UTF-8> when comparing against a
regexp. Forcing Ruby to use binary mode will keep strings as ASCII-8BIT
and seems to work on all current versions of Ruby.

Moreover, there is no way to know for sure which encoding is used
without heuristics or parsing the potential encoding shebang thus binary
mode is probably safer.

[zeha@debian.org: Backport to simplecov 0.7]
---
 lib/simplecov/source_file.rb | 3 +--
 1 file changed, 1 insertion(+), 2 deletions(-)

Index: ruby-simplecov/lib/simplecov/source_file.rb
===================================================================
--- ruby-simplecov.orig/lib/simplecov/source_file.rb	2013-12-24 00:02:51.164975612 +0100
+++ ruby-simplecov/lib/simplecov/source_file.rb	2013-12-24 00:03:11.192909910 +0100
@@ -30,7 +30,6 @@ module SimpleCov
         raise ArgumentError, "Only Fixnum and nil accepted for coverage" unless coverage.kind_of?(Fixnum) or coverage.nil?
         @src, @line_number, @coverage = src, line_number, coverage
         @skipped = false
-        @src.encode!('UTF-8', 'UTF-8', :invalid => :replace) if @src.respond_to?(:encode!)
       end
 
       # Returns true if this is a line that should have been covered, but was not
@@ -79,7 +78,7 @@ module SimpleCov
 
     def initialize(filename, coverage)
       @filename, @coverage = filename, coverage
-      File.open(filename, "r:UTF-8") {|f| @src = f.readlines }
+      File.open(filename, "rb") {|f| @src = f.readlines }
     end
 
     # Returns all source lines for this file as instances of SimpleCov::SourceFile::Line,
